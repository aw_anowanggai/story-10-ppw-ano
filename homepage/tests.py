from django.test import TestCase, Client
from .views import Halaman_LogIn, Welcome_LogIn, Keluar_LogOut
from django.urls import resolve
from django.contrib.auth.models import User

class Story10Test(TestCase):

    def test_login_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_login_url_is_redirect(self):
        response = Client().get('/welcome/')
        self.assertEqual(response.status_code, 302)

    def test_Story10_url_doesnt_exist(self):
        response = Client().get('/datang/')
        self.assertEqual(response.status_code, 404)

    def test_login_func(self):
        found = resolve('/')
        self.assertEqual(found.func, Halaman_LogIn)

    def test_welcome_func2(self):
        found = resolve('/welcome/')
        self.assertEqual(found.func, Welcome_LogIn)

    def test_login_to_do_list_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_welcome_after_login_url(self):
        user = User.objects.create_user('awvenger', 'awvenger@admin.com', 'awpassword')
        self.client.login(username='awvenger', password='awpassword')
        response = self.client.get('/welcome/')
        self.assertEqual(response.status_code, 200)

    def test_welcome_after_login_url_doesnt_exist(self):
        user = User.objects.create_user('awvenger', 'awvenger@admin.com', 'awpassword')
        self.client.login(username='awvenger', password='awpassword')
        response = self.client.get('/masuk/')
        self.assertEqual(response.status_code, 404)

    def test_welcome_after_login_templates(self):
        user = User.objects.create_user('awvenger', 'awvenger@admin.com', 'awpassword')
        self.client.login(username='awvenger', password='awpassword')
        response = self.client.get('/welcome/')
        self.assertTemplateUsed(response, 'welcome.html')
    
    def test_welcome_after_login_redirect(self):
        user = User.objects.create_user('awvenger', 'awvenger@admin.com', 'awpassword')
        self.client.login(username='awvenger', password='awpassword')
        response = self.client.get('/')
        self.assertEqual(response.status_code, 302)
    
    def test_logout_redirect(self):
        user = User.objects.create_user('awvenger', 'awvenger@admin.com', 'awpassword')
        self.client.login(username='awvenger', password='awpassword')
        response = self.client.post('/keluar/')
        self.assertEqual(response.status_code, 302)
    

