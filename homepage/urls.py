from django.contrib import admin
from django.urls import path
from homepage import views

app_name = 'homepage'

urlpatterns = [
    path('', views.Halaman_LogIn), 
    path('welcome/', views.Welcome_LogIn),
    path('keluar/', views.Keluar_LogOut),


]